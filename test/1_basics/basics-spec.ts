/// <reference path="../../node_modules/@types/mocha/index.d.ts" />
import * as chai from "chai";
import {introduce} from "../../src/1_basics/basics";

const expect = chai.expect;

describe("greeter", () => {
  it("should introduce itself like 007", () => {
    expect(introduce('James', 'Bond')).to.equal("My name is Bond... James Bond.");
  });
});
